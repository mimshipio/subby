# Subby

A very simple commandline tool to timeshift subtitles.
It can only take inputs in seconds for the moment (but I might try and add the ability to shift by fractions of seconds in the future).
