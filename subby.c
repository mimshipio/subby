#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv);
const char *suffix(const char path[]);
int shifter(FILE *input_subs, FILE *output_subs);
void file2string(FILE *input_subs, int shift_seconds, FILE *output_subs);
void process_subtitle(char *line, int shift_seconds, FILE *output_subs);
void shift_timestamp(char *timestamp, int shift_seconds);

int main(int argc, char **argv) {
	if (argc < 2) {
		fprintf(stderr, "subby: requires a file operand\n");
		return 1;

	} else if (argc > 2) {
		fprintf(stderr, "subby: only one subtitle file can be edited at a time\n");
		return 1;
	}

	const char *check_srt = suffix(argv[1]);

	FILE *input_subs = fopen(argv[1], "r");
	FILE *output_subs = fopen("output.srt", "w");

	if (input_subs == NULL || output_subs == NULL) {
		fprintf(stderr, "subby: error opening files\n");
		return 1;
	}

	if (strcmp(check_srt, ".srt") == 0) {
		printf("Reading from file\n");
		shifter(input_subs, output_subs);

	} else {
		fprintf(stderr, "subby: file is not a recognised subtitle file\n");
		fprintf(stderr, "recognised formats: .srt\n");
		return 2;
	}

	fclose(input_subs);
	fclose(output_subs);

	remove(argv[1]);
	rename("output.srt", argv[1]);

	return 0;
}

const char *suffix(const char path[]) {
	const char *result;
	int i;
	int n;

	n = strlen(path);
	i = n - 1;
	while ((i > 0) && (path[i] != '.') && (path[i] != '/')) {
		i--;
	}
	if ((i > 0) && (i < n - 1) && (path[i] == '.') && (path[i] != '/')) {
		result = path + i;
	} else {
		result = path + n;
	}

	return result;
}

int shifter(FILE *input_subs, FILE *output_subs) {
	int operation;
	int shift_seconds;

	printf("What operation would you like to perform?:\n");
	printf("Options: [1] shift times forwards, [2] shift times backwards: ");
	scanf("%d", &operation);

	if (operation < 1 || operation > 2) {
		fprintf(stderr, "subby: not a valid operation\n");
		fprintf(stderr, "Please choose a number, 1 or 2\n");
		return 3;
	}

	printf("How many seconds would you like to shift the times? ");
	scanf("%d", &shift_seconds);

	operation == 1 ? (shift_seconds = shift_seconds * 1) : (shift_seconds = shift_seconds * -1);

	printf("Shifting by %d seconds\n", shift_seconds);

	file2string(input_subs, shift_seconds, output_subs);

	return 0;
}

void file2string(FILE *input_subs, int shift_seconds, FILE *output_subs) {
	char *input_lines = NULL;
	size_t length = 0;
	ssize_t read;

	while ((read = getline(&input_lines, &length, input_subs) != -1)) {
		process_subtitle(input_lines, shift_seconds, output_subs);
	}

	free(input_lines);

}

void process_subtitle(char *input_lines, int shift_seconds, FILE *output_subs) {
	if (strstr(input_lines, "-->") != NULL) {
		char start_time[13];
		char end_time[13];
		sscanf(input_lines, "%12s --> %12s", start_time, end_time);

		shift_timestamp(start_time, shift_seconds);
		shift_timestamp(end_time, shift_seconds);

		fprintf(output_subs, "%s --> %s\n", start_time, end_time);

	} else {
		fprintf(output_subs, "%s", input_lines);
	}
}

void shift_timestamp(char *timestamp, int shift_seconds) {
	int hours;
	int minutes;
	int seconds;
	int milliseconds;

	sscanf(timestamp, "%d:%d:%d,%d", &hours, &minutes, &seconds, &milliseconds);

	seconds += shift_seconds;

	while (seconds >= 60) {
		seconds -= 60;
		minutes += 1;
	}
	while (minutes >= 60) {
		minutes -= 60;
		hours += 1;
	}

	while (seconds < 0) {
		seconds += 60;
		minutes -= 1;
	}
	while (minutes < 0) {
		minutes += 60;
		hours -= 1;
	}

	sprintf(timestamp, "%02d:%02d:%02d,%03d", hours, minutes, seconds, milliseconds);
}
